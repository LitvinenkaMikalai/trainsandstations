package org.itstep.qa.runner;

import org.itstep.qa.entity.Speed;
import org.itstep.qa.entity.Train;
import org.itstep.qa.entity.TrainType;

/**
 Существует некий мир в виде линии (одна координата измерения), при этом существующий пошагово.
 В нем есть две станции железной дороги, допустим, Гадюкино и Ведьмино – находятся они на расстоянии 100 единиц друг
 от друга – Гадюкино на координате 0, а Ведьмино на координате 100. Со станции Гадюкино в сторону станции Ведьмино
 по одному за каждый шаг отправляется поезд. Всего отправляется 13 поездов.
 Каждый поезд характеризуют: наименование (Поезд 1, Поезд 2 и т.д.), тип – пассажирский или грузовой, скорость –
 скорость может быть от 1 до 10 единиц за шаг

 Поезда двигаются каждый шаг на расстояние, которое позволяет ему пройти его скорость за шаг. Поезда двигаются
 последовательно: поезд, выехавший на маршрут первым, перемещается первым, второй – вторым и т.д.

 !!! Поезда не могут обгонять друг друга.
 !!! Если своим шагом «собирается» обогнать другой поезд – он не двигается в этот шаг вообще.

 По достижении поездом станции Ведьмино в консоль выводится сообщение:
 «Поезд 1, тип: пассажирский, скорость 5 достиг станции Ведьмино за 5 шагов, моя текущая координата 100», где шаги
 это сколько шагов от начала движения конкретно этого поезда потребовалось ему для достижения конечной станции.
 Значение координаты выводить именно реального положения поезда, а не просто цифру 100.
 Программа работает до тех пор, пока все поезда не достигнут конечной станции. Даже если следующим шагом поезд
 пройдет большее расстояние чем нужно до конечной стации – он останавливается на стации.
 */

public class Main {

    public static void main(String[] args) {
        String nameConstant = "Поезд ";
        int lengthWay = 100;
        int maxCountTrain = 13;
        Speed speed = new Speed();

        // сгенерируем поезда
        Train[] trains = new Train[maxCountTrain];
        for(int i=0; i< trains.length; i++){
            if (i%2 == 0){ // можно былобы генерировать пассажирские/грузовые поезда случайным образом - но фактически на алгоритм это не влияет
                trains[i] = new Train(nameConstant+i,TrainType.CargoTrain,speed.returnTrainSpeed(),0);
            }
            else{
                trains[i] = new Train(nameConstant+i,TrainType.PassengerTrain,speed.returnTrainSpeed(),0);
            }
        }
        int maxCounterTrain = trains.length; // счетчик поездов доехавших до конечной станции
        int step =0; // счетчик шагов
        //
        boolean isRun = true;
        while (isRun){
            step++;
            // пока все поезда не доедут до конечной станции
            // пошагово проедим каждым поездом по пути
            for(int i =0 ; i< trains.length ; i++){
                if(trains[i].getPozicion() != lengthWay) { //он не доехал до конечной станции
                    // если впереди на шаг скорости нет других поездом и недостигнут конец пути - шагаем эти поездом
                    boolean isWayEmpty = true;
                    for (int j=0; j< trains.length; j++){
                        if ( (i!=j) &&  // не сравниваем самого с собой и перед нашим поездом есть другой поезд на шаг пути
                                (trains[i].getPozicion() < trains[j].getPozicion() &&
                                        (trains[i].getPozicion()+ trains[i].getSpeed() >= trains[j].getPozicion()) ) &&
                                (trains[j].getSpeed() != lengthWay ) )  { // за исключением поездов на конечной станции
                            isWayEmpty = false;
                        }
                    }
                    if (isWayEmpty && (trains[i].getPozicion()+ trains[i].getSpeed()) < lengthWay ){ // путь свободен и до конца не доехали - едем дальше
                        trains[i].setPozicion( trains[i].getPozicion()+ trains[i].getSpeed() );
                    }
                    if (isWayEmpty && (trains[i].getPozicion()+ trains[i].getSpeed()) >= lengthWay ){ // путь свободен и доехали до конечной - стоим
                        trains[i].setPozicion(lengthWay);
                        System.out.println(trains[i].toString()+"  достиг станции Ведьмино за "+ step +" шагов, моя текущая координата "+ trains[i].getPozicion());
                        maxCounterTrain --;
                    }

                    if(maxCounterTrain == 0){
                        isRun = false; // все поезда доехали
                    }

                }
            }

        }
        System.out.println ("Все поезда достигли станции Ведьмино за " + step +" шагов");
    }
}
