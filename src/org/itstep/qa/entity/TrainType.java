package org.itstep.qa.entity;

public enum TrainType {
    CargoTrain,
    PassengerTrain
}
