package org.itstep.qa.entity;

/**
 Каждый поезд характеризуют: наименование (Поезд 1, Поезд 2 и т.д.), тип – пассажирский или грузовой, скорость –
 скорость может быть от 1 до 10 единиц за шаг
 */
public class Train {
    private String name;
    private TrainType trainType;
    private int speed;
    private int pozicion;

    public Train(String name, TrainType trainType, int speed,int pozicion) {
        this.name = name;
        this.trainType = trainType;
        this.speed = speed;
        this.pozicion = pozicion;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrainType getTrainType() {
        return trainType;
    }

    public int getPozicion() {
        return pozicion;
    }

    public void setPozicion(int pozicion) {
        this.pozicion = pozicion;
    }

    public void setTrainType(TrainType trainType) {
        this.trainType = trainType;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }


    @Override
    public String toString() {
        return "Поезд{" +
                " имя='" + name + '\'' +
                ", тип=" + trainType +
                ", скорость=" + speed +
                ", поз.=" + pozicion +
                '}';
    }
}
